#!/usr/bin/env bash
set -euo pipefail

# shellcheck source=/dev/null
source /config/config.sh

# validate config
if [ -z "${REPO_URL:-}" ]; then
    die "REPO_URL missing"
fi

# ensure ssh key has correct permissions
ssh_key_path="/config/ssh_private_key"
[ -f "$ssh_key_path" ] && chmod 600 "$ssh_key_path"

# run update once on startup
/usr/local/bin/update

webhook -hooks /config/hooks.json -verbose &

crond -f -l 1
