FROM alpine

RUN apk add bash webhook dumb-init docker-cli-compose age git openssh jq && \
    rm -rf /var/cache/apk

COPY entrypoint.sh /entrypoint
COPY update.sh /usr/local/bin/update
COPY ssh_config /etc/ssh/ssh_config
COPY crontab /var/spool/cron/crontabs/root

ENTRYPOINT [ "dumb-init", "--" ]
CMD [ "/entrypoint" ]
