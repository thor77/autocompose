#!/usr/bin/env bash
set -euo pipefail

# shellcheck source=/dev/null
source /config/config.sh

lock="/tmp/autocompose"
tmp_base="$(mktemp -d)"
prom_tmp="$tmp_base/autocompose.prom"

die() {
    local message="$1"
    echo "$message" >&2
    exit 1
}

onExit() {
    if [ $? -ne 0 ]; then
        echo "autocompose_last_update_success 0" >> "$prom_tmp"
    fi

    if [ -n "${METRIC_DIR:-}" ]; then
        chmod +r "$prom_tmp"
        mv "$prom_tmp" "$METRIC_DIR"
    fi

    rm -rf "$tmp_base"
    rm "$lock"
}

if [ -f "$lock" ]; then
    echo "lock already exists"
    exit 0
fi

touch "$lock"

trap onExit EXIT

repo_dir="/repo"
repo_path_container="$repo_dir/${REPO_SUBPATH:-}"
compose_path="${repo_path_container}/docker-compose.yml"
runtime_compose_path="$tmp_base/docker-compose.yml"

changed="yes"
if [ -d "$repo_dir/.git" ]; then
    echo "repo already cloned, checking for updates"
    git -C "$repo_dir" fetch
    if [[ $(git -C "$repo_dir" rev-parse HEAD) != $(git -C "$repo_dir" rev-parse '@{u}') ]]; then
        git -C "$repo_dir" pull
    else
        changed="no"
    fi
else
    echo "cloning repo"
    git clone "$REPO_URL" "$repo_dir"
fi

if [ ! -f "$compose_path" ]; then
    die "no compose file in repo"
fi

if [[ $changed == "yes" ]]; then
    # decrypt files using age
    readarray -t encrypted_files < <(find "$repo_path_container" -name '*.age')

    if [ "${#encrypted_files[@]}" -ne 0 ]; then
        if [ -z "${AGE_SECRET_KEY:-}" ]; then
            die "encrypted files present but secret key unset"
        fi
        echo "decrypting files using age"
    fi

    for encrypted_file in "${encrypted_files[@]}"; do
        unencrypted_name="${encrypted_file%%.age}"
        age -d -i <(echo "$AGE_SECRET_KEY") "$encrypted_file" > "$unencrypted_name"
    done
else
    echo "no changes"
fi

# find host repo path
repo_path_host_base="$(docker inspect "$HOSTNAME" | jq -r '.[0].Mounts[] | select(.Destination == "/repo") | .Source')"
if [ -z "$repo_path_host_base" ]; then
    die "couldn't determine host repo path"
fi
repo_path_host="$repo_path_host_base/${REPO_SUBPATH:-}"

# replace placeholders in docker compose file
sed -e "s|{{REPO_PATH_HOST}}|$repo_path_host|g" \
    -e "s|{{REPO_PATH_CONTAINER}}|$repo_path_container|g" \
    "$compose_path" > "$runtime_compose_path"

docker compose -f "$runtime_compose_path" -p autocompose up \
    -d --pull=always --remove-orphans --quiet-pull \
    --wait --wait-timeout "${WAIT_TIMEOUT:-30}"

echo "autocompose_last_update_end $(date +%s)" >> "$prom_tmp"
echo "autocompose_last_update_success 1" >> "$prom_tmp"

if [[ "${PRUNE:-true}" == "true" ]]; then
    docker system prune -f
fi
